import sys,os,shutil
import_path = os.path.dirname(os.path.realpath(__file__)) + "/../"
print("Import path: " + import_path)
sys.path.append(import_path)

import unittest
import simplejson as json

from target_json import persist_messages

class NamesTestCase(unittest.TestCase):

    def getFirstFileContent(self):
        path = os.path.dirname(os.path.realpath(__file__)) + "/results/"
        for (dirpath, dirnames, filenames) in os.walk(path):
            if len(filenames) == 0: return ""
            with open(dirpath + filenames[0]) as f:
                return f.read()

    def setUp(self):
        # remove all test results
        path = os.path.dirname(os.path.realpath(__file__))
        shutil.rmtree(path + "/results/")
        os.mkdir(path + "/results/")

    def test_persist_messages_parse_helloworld(self):
        """Test running parser with required schema and input"""
        messages_stream = [target_schema_helloworld_str, target_record_helloworld_str]
        persist_messages(messages_stream, "./test/results/")
        result = json.loads(self.getFirstFileContent())
        self.assertEqual(list(result.keys()), list(['value']))

    def test_persist_messages_parse_inventory(self):
        """Test running parser with required schema and input"""
        messages_stream = [target_schema_inventory_str, target_record_inventory_record_door_1_str]
        persist_messages(messages_stream, "./test/results/")
        result = json.loads(self.getFirstFileContent())
        self.assertEqual(list(result.keys()), list(['checked', 'dimensions', 'id', 'name', 'price', 'tags']))

inventory_schema = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/root.json",
    "type": "object",
    "title": "The Root Schema",
    "description": "The root schema is the schema that comprises the entire JSON document.",
    "default": {},
    "required": [
        "checked",
        "dimensions",
        "id",
        "name",
        "price",
        "tags"
    ],
    "properties": {
        "checked": {
            "$id": "#/properties/checked",
            "type": "boolean",
            "title": "The Checked Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": False,
            "examples": [
                False
            ]
        },
        "dimensions": {
            "$id": "#/properties/dimensions",
            "type": "object",
            "title": "The Dimensions Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": {},
            "examples": [
                {
                    "height": 10.0,
                    "width": 5.0
                }
            ],
            "required": [
                "width",
                "height"
            ],
            "properties": {
                "width": {
                    "$id": "#/properties/dimensions/properties/width",
                    "type": "integer",
                    "title": "The Width Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        5
                    ]
                },
                "height": {
                    "$id": "#/properties/dimensions/properties/height",
                    "type": "integer",
                    "title": "The Height Schema",
                    "description": "An explanation about the purpose of this instance.",
                    "default": 0,
                    "examples": [
                        10
                    ]
                }
            }
        },
        "id": {
            "$id": "#/properties/id",
            "type": "integer",
            "title": "The Id Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                1
            ]
        },
        "name": {
            "$id": "#/properties/name",
            "type": "string",
            "title": "The Name Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": "",
            "examples": [
                "A green door"
            ]
        },
        "price": {
            "$id": "#/properties/price",
            "type": "number",
            "title": "The Price Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": 0,
            "examples": [
                12.5
            ]
        },
        "tags": {
            "$id": "#/properties/tags",
            "type": "array",
            "title": "The Tags Schema",
            "description": "An explanation about the purpose of this instance.",
            "default": [],
            "examples": [
                [
                    "home",
                    "green"
                ]
            ],
            "items": {
                "$id": "#/properties/tags/items",
                "type": "string",
                "title": "The Items Schema",
                "description": "An explanation about the purpose of this instance.",
                "default": "",
                "examples": [
                    "home",
                    "green"
                ]
            }
        }
    }
}
target_inventory_schema = {
    "type":"SCHEMA", 
    "stream":"inventory",
    "key_properties":[],
    "schema": inventory_schema
}

target_inventory_record_door_1 = {
    "checked": False,
    "dimensions": {
        "width": 5,
        "height": 10
    },
    "id": 1,
    "name": "A green door",
    "price": 12.5,
    "tags": [
        "home",
        "green"
    ]
}

inventory_record_door_1 = {
    "type":"RECORD",
    "stream":"inventory",
    "schema":"inventory",
    "record": target_inventory_record_door_1
}

import decimal

class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)

target_schema_inventory_str = json.dumps(target_inventory_schema, use_decimal=True, cls=DecimalEncoder)
target_record_inventory_record_door_1_str = json.dumps(inventory_record_door_1, use_decimal=True, cls=DecimalEncoder)

############################################
#
#   Test data of hello world
#
############################################

target_schema_helloworld = {
    "type":"SCHEMA", 
    "stream":"inventory",
    "key_properties":[],
    "schema":{
        "type":"object", 
        "properties":{
            "value":{
                "type":"string"
            }
        }
    }
}

target_record_helloworld = {
    "type":"RECORD",
    "stream":"inventory",
    "schema":"inventory",
    "record":{
        "value":"world"
    }
}
target_schema_helloworld_str = json.dumps(target_schema_helloworld)
target_record_helloworld_str = json.dumps(target_record_helloworld)


if __name__ == '__main__':
    unittest.main()